#!/usr/bin/env php
<?php

declare(strict_types=1);

use Epicentr\Application\Commands\ImportUsersCommand;

require_once __DIR__ . '/../src/bootstrap.php';

try {
    (new ImportUsersCommand())->execute();
} catch (Throwable $throwable) {
    die();
}