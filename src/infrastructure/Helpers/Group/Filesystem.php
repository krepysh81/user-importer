<?php

declare(strict_types=1);

if (!function_exists('rootDirectory')) {

    /**
     * Get root directory
     *
     * @return string
     */
    function rootDirectory(): string
    {
        return dirname(__DIR__, 4) . DIRECTORY_SEPARATOR;
    }
}