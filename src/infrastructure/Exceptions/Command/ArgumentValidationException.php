<?php

declare(strict_types=1);

namespace Epicentr\Infrastructure\Exceptions\Command;

use Exception;

class ArgumentValidationException extends Exception
{
    protected $message = 'Invalidate argument';
}