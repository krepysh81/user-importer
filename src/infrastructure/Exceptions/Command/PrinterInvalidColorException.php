<?php

declare(strict_types=1);

namespace Epicentr\Infrastructure\Exceptions\Command;

use Exception;

class PrinterInvalidColorException extends Exception
{
    protected $message = 'Color property invalidate';
}