<?php

declare(strict_types=1);

namespace Epicentr\Infrastructure\Commands\Traits;

/**
 * This thread helps to calculate various kinds of information about the script's work.
 */
trait Utils
{
    /**
     * This method helps to calculate the amount of memory used by the script.
     *
     * @return string
     */
    protected function doCalculateMemoryUsage(): string
    {
        return round(memory_get_usage() / 1024 / 1024, 2) . ' MB';
    }

    /**
     * @param float $scriptStartWorkMT
     * @return array
     */
    protected function doCalculateScriptTimeWork(float $scriptStartWorkMT): array
    {
        $diff = microtime(true) - $scriptStartWorkMT;
        $minutes = floor($diff / 60);
        $seconds = $diff % 60;

        return [$minutes, $seconds];
    }
}