<?php

namespace Epicentr\Infrastructure\Commands\Traits;

use Epicentr\Infrastructure\Exceptions\Command\PrinterInvalidColorException;

/**
 * This trade helps to work with the output to the console
 */
trait Printer
{
    /**
     * @var array|string[]
     */
    private static array $foregroundColors = [
        'bold'         => '1',     'dim'          => '2',
        'black'        => '0;30',  'dark_gray'    => '1;30',
        'blue'         => '0;34',  'light_blue'   => '1;34',
        'green'        => '0;32',  'light_green'  => '1;32',
        'cyan'         => '0;36',  'light_cyan'   => '1;36',
        'red'          => '0;31',  'light_red'    => '1;31',
        'purple'       => '0;35',  'light_purple' => '1;35',
        'brown'        => '0;33',  'yellow'       => '1;33',
        'light_gray'   => '0;37',  'white'        => '1;37',
        'normal'       => '0;39',
    ];

    /**
     * @var array|string[]
     */
    private static array $backgroundColors = [
        'black'        => '40',   'red'          => '41',
        'green'        => '42',   'yellow'       => '43',
        'blue'         => '44',   'magenta'      => '45',
        'cyan'         => '46',   'light_gray'   => '47',
    ];

    /**
     * @var array|string[]
     */
    private static array $options = [
        'underline'    => '4',    'blink'         => '5',
        'reverse'      => '7',    'hidden'        => '8',
    ];

    /**
     * Print console line
     */
    public function printLine(): void
    {
        $this->printMessage(str_repeat('-', 60));
    }

    /**
     * @param string $message
     */
    public function printInfoMessage(string $message = ''): void
    {
        $this->printMessage($message);
    }

    /**
     * @param string $message
     */
    public function printErrorMessage(string $message = ''): void
    {
        $this->printMessage($message, 'red');
    }

    /**
     * @param string $message
     */
    public function printSuccessMessage(string $message = ''): void
    {
        $this->printMessage($message, 'green');
    }

    /**
     * General method print in console
     *
     * @param string $message
     * @param string $color
     * @param bool $newline
     * @param string|null $background_color
     */
    public function printMessage(
        string $message = '',
        string $color = 'normal',
        bool $newline = true,
        ?string $background_color = null
    ): void
    {
        if (is_bool($color)) {
            $newline = $color;
            $color   = 'normal';
        } elseif(is_string($color) && is_string($newline)) {
            $background_color = $newline;
            $newline          = true;
        }

        $str = $newline ? $message . PHP_EOL : $message;

        echo self::$color($str, $background_color);
    }

    /**
     * @param string $foregroundColor
     * @param array $args
     * @return string
     * @throws PrinterInvalidColorException
     */
    public static function __callStatic(string $foregroundColor, array $args): string
    {
        $string         = $args[0];
        $coloredString = "";

        // Check if given foreground color found
        if (!isset(self::$foregroundColors[$foregroundColor]) ) {
            throw new PrinterInvalidColorException($foregroundColor . ' not a valid color');
        }

        $coloredString .= "\033[" . self::$foregroundColors[$foregroundColor] . "m";

        array_shift($args);

        foreach ($args as $option){
            // Check if given background color found
            if (isset(self::$backgroundColors[$option])) {
                $coloredString .= "\033[" . self::$backgroundColors[$option] . "m";
            }
            elseif (isset(self::$options[$option])) {
                $coloredString .= "\033[" . self::$options[$option] . "m";
            }
        }

        // Add string and end coloring
        $coloredString .= $string . "\033[0m";

        return $coloredString;

    }

    /**
     * Call console notification bell sound
     *
     * @param int $count
     */
    public function bell(int $count = 1): void
    {
        echo str_repeat("\007", $count);
    }
}