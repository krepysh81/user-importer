<?php

declare(strict_types=1);

namespace Epicentr\Infrastructure\Commands;

use Epicentr\Infrastructure\Commands\Traits\Printer;
use Epicentr\Infrastructure\Commands\Traits\Utils;

abstract class BaseCommand
{
    use Printer;
    use Utils;

    /**
     * A check constant to understand where the script is called from
     */
    private const PHP_CLI_MODE = 'cli';

    /**
     * List of required parameters that need to be obtained
     *
     * @var array
     */
    protected static array $arguments = [];

    /**
     * After success execute command call this method
     *
     * @return mixed
     */
    abstract protected function handle() :void;

    /**
     * Command launch point
     */
    public function execute(): void
    {
        $this->prepareArguments();
        $this->handle();
    }

    /**
     * @param string $argument
     * @return mixed|null
     */
    public function getArgument(string $argument): ?string
    {
        if (isset(static::$arguments[$argument])) {
            return static::$arguments[$argument];
        }

        return null;
    }

    /**
     * Prepare console arguments
     */
    private function prepareArguments(): void
    {
        if (PHP_SAPI !== self::PHP_CLI_MODE) {
            return;
        }

        if (empty(static::$arguments)) {
           return;
        }

        $optOptions = [];

        foreach (static::$arguments as $argument) {
            $optOptions[] = $argument . ":";
        }

        static::$arguments = getopt('', $optOptions);
    }
}