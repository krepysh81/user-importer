<?php

declare(strict_types=1);

namespace Epicentr\Application\Commands;

use Epicentr\Application\Exceptions\Filesystem\FileNotFountException;
use Epicentr\Application\Exceptions\Validation\FileExtensionValidationException;
use Epicentr\Application\Services\UserService;
use Epicentr\Infrastructure\Commands\BaseCommand;
use Throwable;

class ImportUsersCommand extends BaseCommand
{
    /**
     * Script arguments
     *
     * @example php import-users --file=file_patch
     * @var array|string[]
     */
    protected static array $arguments = ['file'];

    /**
     * @var float
     */
    private float $startWorkTime;

    /**
     * @var UserService
     */
    private UserService $userService;

    /**
     * Import users command constructor
     */
    public function __construct()
    {
        $this->userService = new UserService();
    }

    /**
     * @throws FileExtensionValidationException
     * @throws FileNotFountException
     * @throws Throwable
     */
    public function handle(): void
    {
        try {

            // Start work script
            $this->beforeStartCommand();

            // Process import users
            $this->onProcess();

            // After success work
            $this->afterFinishCommand();

        } catch (Throwable $exception) {

            $this->printErrorMessage($exception->getMessage());
            $this->bell();

            throw $exception;

        }

        $this->bell();
    }


    /**
     * @throws FileExtensionValidationException
     * @throws FileNotFountException
     */
    private function onProcess(): void
    {
        $this->userService->importFromCSV($this->getArgument('file'), function (string $infoStep) {
            $this->printMessage(sprintf('Step %s', $infoStep), 'yellow');
            $this->printMessage(sprintf('Memory used %s', $this->doCalculateMemoryUsage()), 'purple');
            $this->printLine();
        });
    }

    /**
     * Before start command logic
     */
    private function beforeStartCommand(): void
    {
        $this->startWorkTime = microtime(true);

        $this->printLine();
        $this->printInfoMessage("Start import users from " . $this->getArgument('file'));
        $this->printLine();
    }

    /**
     * After
     */
    private function afterFinishCommand(): void
    {
        list($workInMinutes, $workInSeconds) = $this->doCalculateScriptTimeWork($this->startWorkTime);

        $this->printSuccessMessage("Import users finished success.");
        $this->printSuccessMessage("Script execution time: {$workInMinutes} minutes {$workInSeconds} seconds");
        $this->printLine();
    }

}