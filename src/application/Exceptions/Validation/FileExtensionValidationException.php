<?php

declare(strict_types=1);

namespace Epicentr\Application\Exceptions\Validation;

use Exception;

class FileExtensionValidationException extends Exception
{
    protected $message = 'Invalidate file extension';
}