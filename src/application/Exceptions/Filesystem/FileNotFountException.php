<?php

declare(strict_types=1);

namespace Epicentr\Application\Exceptions\Filesystem;

use Exception;

class FileNotFountException extends Exception
{
    protected $message = "File not found";
}