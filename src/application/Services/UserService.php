<?php

declare(strict_types=1);

namespace Epicentr\Application\Services;

use Epicentr\Application\Exceptions\Filesystem\FileNotFountException;
use Epicentr\Application\Exceptions\Validation\FileExtensionValidationException;
use Epicentr\Application\Facade\Importer\FileReader\CSVFile;
use Epicentr\Application\Facade\Importer\ImporterFacade;
use LimitIterator;
use SplFileObject;

final class UserService
{
    private const FOLDER_UPLOAD_IMPORT_RESULT = 'resources/result';
    private const DEFAULT_FILE_NAME_RESULT    = 'result.csv';

    /**
     * @var ImporterFacade
     */
    private ImporterFacade $importerFacade;

    /**
     * User service constructor
     */
    public function __construct()
    {
        $this->importerFacade = new ImporterFacade();
    }

    /**
     * @param string|null $filePath
     * @param callable $processImport
     * @throws FileExtensionValidationException
     * @throws FileNotFountException
     */
    public function importFromCSV(?string $filePath, callable $processImport): void
    {
        $this->importerFacade->setReader(
            new CSVFile($filePath, CSVFile::CSV_CONTROL_COMMA)
        );

        $this->processImportFromCSV(
            $this->importerFacade->getData(),
            $processImport
        );
    }

    /**
     * @param SplFileObject $data
     * @param callable $processImport
     */
    private function processImportFromCSV(SplFileObject $data, callable $processImport): void
    {
        $processImport('1. Start build tree');

        $tree = [];

        $this->processBuildUsersTree($data, $tree);

        $processImport('2. Done build tree');

        $processImport('3. Start create result file');

        $this->processGenerateResult($data, $tree);

        $processImport('4. Done create file');
    }

    /**
     * Process build user tree
     *
     * @param SplFileObject $data
     * @param array $tree
     */
    private function processBuildUsersTree(SplFileObject $data, array &$tree): void
    {
        // Ignore first header row
        $rows = new LimitIterator($data, 1);

        foreach ($rows as $row) {
            list($id, $parentId, $email, $card, $phone) = $row;

            $tree['emails'][$email][] = (int) $id;
            $tree['cards'][$card][]   = (int) $id;
            $tree['phones'][$phone][] = (int) $id;
        }
    }

    /**
     * Process generate import file result
     *
     * @param SplFileObject $data
     * @param array $tree
     */
    private function processGenerateResult(SplFileObject $data, array $tree): void
    {
        $file = $this->createNewFileResult();

        foreach ($data as $index => $row) {

            list($id, $parentId, $email, $card, $phone) = $row;

            // Set default parent id is current row
            $parentId   = $index !== 0 ? $id : $parentId;

            // Check if first iteration has match
            $firstMatch = false;

            if (isset($tree['emails'][$email]) && count($tree['emails'][$email]) > 1) {
                $parentId = min($tree['emails'][$email]);
                $firstMatch = true;
            }

            if (!$firstMatch && isset($tree['phones'][$phone]) && count($tree['phones'][$phone]) > 1) {
                $parentId = min($tree['phones'][$phone]);
            }

            if (!$firstMatch && isset($tree['cards'][$card]) && count($tree['cards'][$card]) > 1) {
                $parentId = min($tree['cards'][$card]);
            }

            $file->fputcsv([$id, $parentId, $email, $card, $phone]);
        }
    }

    /**
     * @return SplFileObject
     */
    private function createNewFileResult(): SplFileObject
    {
        return new SplFileObject(
            rootDirectory()
            . self::FOLDER_UPLOAD_IMPORT_RESULT
            . DIRECTORY_SEPARATOR
            . self::DEFAULT_FILE_NAME_RESULT,
            'w'
        );
    }
}