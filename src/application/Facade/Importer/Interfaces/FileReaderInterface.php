<?php

declare(strict_types=1);

namespace Epicentr\Application\Facade\Importer\Interfaces;

interface FileReaderInterface
{
    /**
     * @return mixed
     */
    public function validate(): void;

    /**
     * @return mixed
     */
    public function getData();
}