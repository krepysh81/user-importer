<?php

declare(strict_types=1);

namespace Epicentr\Application\Facade\Importer\Interfaces;

interface ImporterInterface
{
    /**
     * @param FileReaderInterface $fileReader
     * @return mixed
     */
    public function setReader(FileReaderInterface $fileReader);

    /**
     * @return mixed
     */
    public function getData();
}