<?php

declare(strict_types=1);

namespace Epicentr\Application\Facade\Importer;

use Epicentr\Application\Facade\Importer\Interfaces\FileReaderInterface;
use Epicentr\Application\Facade\Importer\Interfaces\ImporterInterface;
use SplFileObject;

class ImporterFacade implements ImporterInterface
{
    /**
     * @var FileReaderInterface
     */
    private FileReaderInterface $fileReader;

    /**
     * @param FileReaderInterface $fileReader
     */
    public function setReader(FileReaderInterface $fileReader): void
    {
        $this->fileReader = $fileReader;
        $fileReader->validate();
    }

    /**
     * @return SplFileObject
     */
    public function getData(): SplFileObject
    {
       return $this->fileReader->getData();
    }
}