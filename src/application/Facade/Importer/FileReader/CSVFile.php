<?php

declare(strict_types=1);

namespace Epicentr\Application\Facade\Importer\FileReader;

use Epicentr\Application\Exceptions\Filesystem\FileNotFountException;
use Epicentr\Application\Exceptions\Validation\FileExtensionValidationException;
use Epicentr\Application\Facade\Importer\Interfaces\FileReaderInterface;
use SplFileObject;

class CSVFile implements FileReaderInterface
{
    public const CSV_CONTROL_SEMICOLON = ';';
    public const CSV_CONTROL_COMMA = ',';

    private const FILE_EXTENSION = 'csv';

    /**
     * @var ?string
     */
    private ?string $filePath;

    /**
     * @var string
     */
    private string $csvControl;

    /**
     * @var SplFileObject
     */
    private SplFileObject $file;

    /**
     * @param ?string $filePath
     * @param string $csvControl
     * @throws FileExtensionValidationException
     * @throws FileNotFountException
     */
    public function __construct(?string $filePath, string $csvControl = self::CSV_CONTROL_SEMICOLON)
    {
        $this->filePath = $filePath;
        $this->csvControl = $csvControl;

        $this->validate();

        $this->file = new SplFileObject($filePath);
    }

    public function validate(): void
    {
        if (empty($this->filePath)) {
            throw new FileNotFountException();
        }

        $filePath = rootDirectory() . $this->filePath;

        if (!file_exists($filePath)) {
            throw new FileNotFountException(sprintf('File not fount in: %s', $filePath));
        }

        $pathParts = pathinfo($this->filePath);

        if ($pathParts['extension'] !== self::FILE_EXTENSION) {
            throw new FileExtensionValidationException();
        }
    }

    /**
     * Get data from csv file
     *
     * @return SplFileObject
     */
    public function getData(): SplFileObject
    {
        $this->file->setFlags(SplFileObject::READ_CSV
            | SplFileObject::SKIP_EMPTY
            | SplFileObject::READ_AHEAD
            | SplFileObject::DROP_NEW_LINE
        );

        $this->file->seek(PHP_INT_MAX);
        $this->file->setCsvControl($this->csvControl);

       return $this->file;
    }
}